package com.wojtek.CarService.controllers;

import com.wojtek.CarService.entities.Service;
import com.wojtek.CarService.repositories.ServiceRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AdminController {

    private ServiceRepository serviceRepository;

    public AdminController(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @GetMapping("/admin")
    public String addService() {
        return "admin/admin";
    }

    @PostMapping("/admin/add")
    public String add(@RequestParam("kind") String kind, @RequestParam("type") String type,
                      @RequestParam("name") String name, @RequestParam("price") double price,
                      @RequestParam("quantity") int quantity, Model model) {

        Service service = new Service().kind(kind).type(type).name(name).price(price).quantity(quantity);
        model.addAttribute("kind", kind);
        serviceRepository.save(service);
        return "admin/add";
    }


}
