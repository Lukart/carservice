package com.wojtek.CarService.controllers;

import com.wojtek.CarService.repositories.ServiceRepository;
import com.wojtek.CarService.services.CarService;
import com.wojtek.CarService.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RentController {

    private CarService carService;
    private OrderService orderService;
    private ServiceRepository serviceRepository;

    public RentController(CarService carService, OrderService orderService, ServiceRepository serviceRepository) {
        this.carService = carService;
        this.orderService = orderService;
        this.serviceRepository = serviceRepository;
    }

    @GetMapping("/rentCar")
    public String rentCar(Model model) {
        carService.display("rent", "car", model);
        return "rent/rentCar";

    }

    @GetMapping("/rentTrailer")
    public String rentTrailer(Model model) {
        carService.display("rent", "trailer", model);
        return "rent/rentTrailer";
    }

    @GetMapping("/orderRent")
    public String order() {
        return "rent/orderRent";
    }

    @PostMapping("/orderRent")
    public String order(@RequestParam("html") String orderHtml, Model model) {
        int id = Integer.parseInt(orderService.getid(orderHtml));
        com.wojtek.CarService.entities.Service service = serviceRepository.findById(id);
        model.addAttribute("service", service);
        return "rent/orderRent";
    }

    @PostMapping("/orderRent/confirmation")
    public String confirm(@RequestParam("id") int id, @RequestParam("name") String name,
                          @RequestParam("price") double price, @RequestParam("date") String date,
                          @RequestParam("time") int time, Model model) {
        if (orderService.confirmOrder(id, name, price, date, time, model)) return "error";
        return "rent/rentConfirmation";
    }

}