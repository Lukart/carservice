package com.wojtek.CarService.controllers;

import com.wojtek.CarService.repositories.ServiceRepository;
import com.wojtek.CarService.services.CarService;
import com.wojtek.CarService.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class RepairController {

    private CarService carService;
    private OrderService orderService;
    private ServiceRepository serviceRepository;

    public RepairController(CarService carService, OrderService orderService, ServiceRepository serviceRepository) {
        this.carService = carService;
        this.orderService = orderService;
        this.serviceRepository = serviceRepository;
    }

    @GetMapping("/repairBody")
    public String saleBody(Model model) {
        carService.display("repair", "body", model);
        return "repair/repairBody";
    }

    @GetMapping("/repairBrake")
    public String saleBrake(Model model) {
        carService.display("repair", "brake", model);
        return "repair/repairBrake";
    }

    @GetMapping("/repairCooling")
    public String saleCooling(Model model) {
        carService.display("repair", "cooling", model);
        return "repair/repairCooling";
    }

    @GetMapping("/repairElectrical")
    public String saleElectrical(Model model) {
        carService.display("repair", "electrical", model);
        return "repair/repairElectrical";
    }

    @GetMapping("/repairEngine")
    public String saleEngine(Model model) {
        carService.display("repair", "engine", model);
        return "repair/repairEngine";
    }

    @GetMapping("/repairSteeringSystem")
    public String saleSteeringSystem(Model model) {
        carService.display("repair", "steeringsystem", model);
        return "repair/repairSteeringSystem";
    }

    @GetMapping("/repairSuspension")
    public String saleSuspension(Model model) {
        carService.display("repair", "suspension", model);
        return "repair/repairSuspension";
    }

    @PostMapping("/orderRepair")
    public String order(@RequestParam("html") String orderHtml, Model model) {
        int id = Integer.parseInt(orderService.getid(orderHtml));
        com.wojtek.CarService.entities.Service service = serviceRepository.findById(id);
        model.addAttribute("service", service);
        return "repair/orderRepair";
    }

    @PostMapping("/orderRepair/confirmation")
    public String confirm(@RequestParam("id") int id, @RequestParam("name") String name,
                          @RequestParam("price") double price, @RequestParam("date") String date,
                          @RequestParam("time") int time, Model model) {
        if (orderService.confirmOrder(id, name, price, date, time, model)) return "error";
        return "repair/repairConfirmation";
    }
}
