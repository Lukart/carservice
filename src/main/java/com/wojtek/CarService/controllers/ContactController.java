package com.wojtek.CarService.controllers;

import com.wojtek.CarService.services.ContactService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import java.time.DayOfWeek;
import java.time.LocalDateTime;

@Controller
public class ContactController {

    private ContactService contactService;

    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    @GetMapping("/start")
    public String start() {
        return "start";
    }

    @GetMapping("/contact")
    public String contact(Model model) {
        DayOfWeek dayOfWeek = LocalDateTime.now().getDayOfWeek();

        String date = contactService.getDate();
        String time = contactService.getTime();
        boolean state = contactService.state(dayOfWeek);

        model.addAttribute("dayOfWeek", dayOfWeek);
        model.addAttribute("state", state);
        model.addAttribute("date", date);
        model.addAttribute("time", time);

        return "contact";
    }
}
