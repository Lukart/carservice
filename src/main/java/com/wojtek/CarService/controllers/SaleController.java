package com.wojtek.CarService.controllers;

import com.wojtek.CarService.repositories.ServiceRepository;
import com.wojtek.CarService.services.CarService;
import com.wojtek.CarService.services.OrderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class SaleController {

    private CarService carService;
    private OrderService orderService;
    private ServiceRepository serviceRepository;

    public SaleController(CarService carService, OrderService orderService, ServiceRepository serviceRepository) {
        this.carService = carService;
        this.orderService = orderService;
        this.serviceRepository = serviceRepository;
    }

    @GetMapping("/saleBody")
    public String saleBody(Model model) {
        carService.display("sale", "body", model);
        return "sale/saleBody";
    }

    @GetMapping("/saleBrake")
    public String saleBrake(Model model) {
        carService.display("sale", "brake", model);
        return "sale/saleBrake";
    }

    @GetMapping("/saleCooling")
    public String saleCooling(Model model) {
        carService.display("sale", "cooling", model);
        return "sale/saleCooling";
    }

    @GetMapping("/saleElectrical")
    public String saleElectrical(Model model) {
        carService.display("sale", "electrical", model);
        return "sale/saleElectrical";
    }

    @GetMapping("/saleEngine")
    public String saleEngine(Model model) {
        carService.display("sale", "engine", model);
        return "sale/saleEngine";
    }

    @GetMapping("/saleSteeringSystem")
    public String saleSteeringSystem(Model model) {
        carService.display("sale", "steeringsystem", model);
        return "sale/saleSteeringSystem";
    }

    @GetMapping("/saleSuspension")
    public String saleSuspension(Model model) {
        carService.display("sale", "suspension", model);
        return "sale/saleSuspension";
    }

    @GetMapping("/orderSale")
    public String orderGet() {
        return "sale/orderSale";
    }

    @PostMapping("/orderSale")
    public String order(@RequestParam("html") String orderHtml, Model model) {
        int id = Integer.parseInt(orderService.getid(orderHtml));
        com.wojtek.CarService.entities.Service service = serviceRepository.findById(id);
        model.addAttribute("service", service);
        return "sale/orderSale";
    }

    @GetMapping("/orderSale/confirmation")
    public String confirmGet() {
        return "sale/saleConfirmation";
    }

    @PostMapping("/orderSale/confirmation")
    public String confirm(@RequestParam("id") int id, @RequestParam("name") String name,
                          @RequestParam("price") double price, @RequestParam("quantity") int quantity, Model model) {
        if (orderService.confirmSaleOrder(id, name, price, quantity, model)) return "error";
        return "sale/saleConfirmation";
    }
}
