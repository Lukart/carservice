package com.wojtek.CarService.controllers;

import com.wojtek.CarService.entities.Address;
import com.wojtek.CarService.entities.User;
import com.wojtek.CarService.entities.UserAuthority;
import com.wojtek.CarService.repositories.AddressRepository;
import com.wojtek.CarService.repositories.UserAuthorityRepository;
import com.wojtek.CarService.repositories.UserRepository;
import com.wojtek.CarService.services.EmailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import javax.mail.*;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;

@Controller
public class RegisterController {

    @Value("${email.address.register.to}")
    private String registerMail;

    @Autowired
    JdbcTemplate jdbcTemplate;

    @Autowired
    PasswordEncoder passwordEncoder;

    private EmailService emailService;
    private AddressRepository addressRepository;
    private UserRepository userRepository;
    private UserAuthorityRepository userAuthorityRepository;

    public RegisterController(EmailService emailService, AddressRepository addressRepository, UserRepository userRepository, UserAuthorityRepository userAuthorityRepository) {
        this.emailService = emailService;
        this.addressRepository = addressRepository;
        this.userRepository = userRepository;
        this.userAuthorityRepository = userAuthorityRepository;
    }

    @GetMapping("/registration")
    public String register() {
        return "registration";
    }


    @GetMapping("/postRegistration")
    public String postRegistration() {
        return "postRegistration";
    }

    @PostMapping("/registration")
    public String register(@RequestParam("firstName") String firstName,
                           @RequestParam("lastName") String lastName, @RequestParam("city") String city,
                           @RequestParam("street") String street, @RequestParam("number") long number,
                           @RequestParam("code") String code, @RequestParam("email") String emailTo,
                           @RequestParam("username") String username, @RequestParam("password") String password,
                           @RequestParam("password2") String password2, HttpServletRequest httpServletRequest,
                           Model model) throws MessagingException, IOException, ServletException {

        if (!password.equals(password2)) {
            model.addAttribute("errorpassword", true);
            return "registration";
        }
        if (userRepository.findByUsername(username) != null) {
            model.addAttribute("erroruser", true);
            return "registration";
        }
        String encodedPassword = passwordEncoder.encode(password);
        model.addAttribute("firstName", firstName);
        model.addAttribute("lastName", lastName);
        model.addAttribute("city", city);
        model.addAttribute("street", street);
        model.addAttribute("number", number);
        model.addAttribute("code", code);
        model.addAttribute("email", emailTo);
        model.addAttribute("username", username);
        Address address = new Address().city(city).code(code).street(street).number(number);
        User user = new User().firstName(firstName).lastName(lastName).username(username).password(encodedPassword).email(emailTo).address(address).enabled(true);
        addressRepository.save(address);
        userRepository.save(user);
        UserAuthority userAuthority = new UserAuthority().username(username).authority("ROLE_USER");
        userAuthorityRepository.save(userAuthority);
        String mailSubject = "Rejestracja CarService";
        String mailContent = "Zarejestrowales sie w CarService. Twoj login to: ";
        emailService.sendmail(registerMail, mailSubject, mailContent + username);
        emailService.sendmail(emailTo, mailSubject, mailContent + username);
        httpServletRequest.login(username, password);
        return "postRegistration";
    }
}
