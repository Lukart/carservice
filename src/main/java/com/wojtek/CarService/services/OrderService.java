package com.wojtek.CarService.services;

import com.wojtek.CarService.entities.Orders;
import com.wojtek.CarService.entities.User;
import com.wojtek.CarService.repositories.OrdersRepository;
import com.wojtek.CarService.repositories.ServiceRepository;
import com.wojtek.CarService.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;

@Service
public class OrderService {
    private Date now = Date.from(Instant.now());

    @Value("${email.address.orders.to}")
    private String orderMail;

    private ServiceRepository serviceRepository;
    private UserRepository userRepository;
    private EmailService emailService;
    private OrdersRepository ordersRepository;

    public OrderService(ServiceRepository serviceRepository, UserRepository userRepository, EmailService emailService, OrdersRepository ordersRepository) {
        this.serviceRepository = serviceRepository;
        this.userRepository = userRepository;
        this.emailService = emailService;
        this.ordersRepository = ordersRepository;
    }

    public String getid(String html) {
        int index = 0;
        int counter = 0;
        for (int i = 0; i < html.length(); i++) {
            if (html.charAt(i) == '<') {
                counter++;
                if (counter == 2) {
                    index = i;
                    break;
                }
            }
        }
        return html.substring(html.indexOf('>') + 1, index);
    }

    public boolean confirmOrder(int id, String name, double price, String serviceDate, int time, Model model) {
        String username = getUsername();
        User user = userRepository.findByUsername(username);
        Orders orders = new Orders().orderDate(getDate()).serviceDate(serviceDate).user(user).serviceTime(time).service(serviceRepository.findById(id));
        ordersRepository.save(orders);
        String emailTo = user.email();
        setAttributes(model, username, emailTo);
        String content = "id " + id + "/nazwa " + name + "/cena " + price + "/Data " + serviceDate + "/Godz. " + time;
        return sendEmail(model, emailTo, content);
    }

    private String getDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy/MM/dd");
        return dateFormatter.format(now);
    }

    public boolean confirmSaleOrder(int id, String name, double price, int quantity, Model model) {
        String username = getUsername();
        User user = userRepository.findByUsername(username);
        try {
            com.wojtek.CarService.entities.Service service = getService(id, quantity);
            Orders orders = new Orders().orderDate(getDate()).quantity(quantity).user(user).service(service);
            ordersRepository.save(orders);
        } catch (RuntimeException r) {
            model.addAttribute("error", "Ilosc sztuk przekracza stan magazynowy");
            return true;
        }
        String emailTo = user.email();
        String content = "id " + id + "/nazwa " + name + "/cena " + price + "/ilosc" + quantity;
        setAttributes(model, username, emailTo);
        return sendEmail(model, emailTo, content);
    }

    private synchronized com.wojtek.CarService.entities.Service getService(int id, int quantity) {
        com.wojtek.CarService.entities.Service service = serviceRepository.findById(id);
        if (service == null || quantity > service.quantity()) {
            throw new RuntimeException();
        }
        int newQuantity = service.quantity() - quantity;
        service.quantity(newQuantity);
        serviceRepository.save(service);
        return service;
    }

    private void setAttributes(Model model, String username, String emailTo) {
        model.addAttribute("username", username);
        model.addAttribute("emailTo", emailTo);
    }

    private boolean sendEmail(Model model, String emailTo, String content) {
        try {
            emailService.sendmail(orderMail, "Zamowienie", content);
            emailService.sendmail(emailTo, "Zamowienie", content);
        } catch (MessagingException | IOException e) {
            model.addAttribute("error", "Wystąpił błąd. Wiadomość nie została wysłana");
            return true;
        }
        return false;
    }

    private String getUsername() {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        return auth.getName();
    }

}
