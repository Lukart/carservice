package com.wojtek.CarService.services;


import com.wojtek.CarService.entities.Orders;
import com.wojtek.CarService.repositories.OrdersRepository;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.mail.MessagingException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

@Component
public class MailNotifications {

    private OrdersRepository ordersRepository;
    private EmailService emailService;

    public MailNotifications(OrdersRepository ordersRepository, EmailService emailService) {
        this.ordersRepository = ordersRepository;
        this.emailService = emailService;
    }

    @Scheduled(cron = "${cron.expression}")
    public void sendNotifications() throws IOException, MessagingException {
        String stringTomorrow = getTomorrow();
        String subject = "Powiadomienie o jutrzejszej wizycie";
        sendMail(stringTomorrow, subject);
    }

    private void sendMail(String stringTomorrow, String subject) throws MessagingException, IOException {
        String mail;
        String content;
        List<Orders> ordersToNotify = ordersRepository.findAllByServiceDateEquals(stringTomorrow);
        for (Orders order : ordersToNotify) {
            mail = order.user().email();
            content = order.service().kind() + "/" + order.service().type() + "/" + order.service().name();
            emailService.sendmail(mail, subject, content);
        }
    }

    private String getTomorrow() {
        Calendar calendar = Calendar.getInstance();
        calendar.getTime();
        calendar.add(Calendar.DAY_OF_YEAR, 1);
        Date tomorrow = calendar.getTime();
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");
        return dateFormatter.format(tomorrow);
    }

}

