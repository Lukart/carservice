package com.wojtek.CarService.services;

import com.wojtek.CarService.models.ExchangeRates;
import com.wojtek.CarService.models.Rate;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;

@Service
public class ExchangeRatesService {

    @Value("${baseCurrency}")
    private String baseCurrency;

    @Value("${currency}")
    private String currency;

    private final String url = "https://api.exchangeratesapi.io/latest?base=";

    private RestTemplate restTemplate;

    public ExchangeRatesService(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    public double getRate() {
        return getRates().stream()
                .filter(rate -> rate.getName().equals(currency))
                .map(Rate::getRate).findFirst().orElseThrow(() -> new NoSuchElementException(" Brak takiej waluty!!!"));
    }

    private List<Rate> getRates() {
        ExchangeRates exchangeRates = restTemplate.getForObject(url + baseCurrency, ExchangeRates.class);
        List<Rate> rates = new ArrayList<>();
        for (Map.Entry<String, Double> entry : exchangeRates.rates.entrySet()) {
            rates.add(new Rate(entry.getKey(), entry.getValue()));
        }
        return rates;
    }
}
