package com.wojtek.CarService.services;

import com.wojtek.CarService.repositories.ServiceRepository;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.ui.Model;

import java.util.List;

@Service
public class CarService {

    @Value("${currency}")
    private String currency;

    @Value("${spread}")
    private double spread;

    private ServiceRepository serviceRepository;
    private ExchangeRatesService exchangeRatesService;

    public CarService(ServiceRepository serviceRepository, ExchangeRatesService exchangeRatesService) {
        this.serviceRepository = serviceRepository;
        this.exchangeRatesService = exchangeRatesService;
    }

    public void display(String kind, String type, Model model) {
        List<com.wojtek.CarService.entities.Service> vehicleList = serviceRepository.findAllByKindAndTypeOrderByName(kind, type);
        double rate = exchangeRatesService.getRate();
        double askValue = rate + rate * spread;
        model.addAttribute("vehicleList", vehicleList);
        model.addAttribute("currency", currency);
        model.addAttribute("askValue", askValue);
    }
}
