package com.wojtek.CarService.services;

import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.util.Date;

@Service
public class ContactService {

    private Date now = Date.from(Instant.now());

    public String getTime() {
        SimpleDateFormat timeFormatter = new SimpleDateFormat(" HH:mm:ss");
        return timeFormatter.format(now);
    }

    private String getHour() {
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HH");
        return timeFormatter.format(now);
    }

    private String getSec() {
        SimpleDateFormat timeFormatter = new SimpleDateFormat("ss");
        return timeFormatter.format(now);
    }

    public String getDate() {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("dd/MM/yyyy");
        return dateFormatter.format(now);
    }

    public boolean state(DayOfWeek dayOfWeek) {
        int hour = Integer.parseInt(getHour().trim());
        int sec = Integer.parseInt(getSec().trim());
        return (dayOfWeek.equals(DayOfWeek.SATURDAY)
                || dayOfWeek.equals(DayOfWeek.SUNDAY)
                || hour < 10 || hour > 18 || (hour == 18 && sec > 0));
    }
}
