package com.wojtek.CarService.api;

import com.google.common.collect.Lists;
import com.wojtek.CarService.entities.Service;
import com.wojtek.CarService.repositories.ServiceRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class ApiController {

    private static final String URL = "/api/facilitate";

    private ServiceRepository serviceRepository;

    public ApiController(ServiceRepository serviceRepository) {
        this.serviceRepository = serviceRepository;
    }

    @GetMapping(URL)
    private List<ShowService> facilitate() {
        List<ShowService> serviceList = new ArrayList<>();
        List<Service> displayPrices = Lists.newArrayList(serviceRepository.findAll());
        for (Service service : displayPrices) {
            serviceList.add(new ShowService(service));
        }
        return serviceList;
    }

}
