package com.wojtek.CarService.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;

@Data
@Accessors(fluent = true)
@Entity
@Table(name = "orders")
public class Orders {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "orderDate", nullable = false)
    private String orderDate;

    @Column(name = "serviceDate")
    private String serviceDate;

    @Column(name = "serviceTime")
    private Integer serviceTime;

    @Column(name = "quantity")
    private Integer quantity;

    @ManyToOne(fetch = FetchType.EAGER)
    private User user;

    @ManyToOne(fetch = FetchType.EAGER)
    private Service service;
}
