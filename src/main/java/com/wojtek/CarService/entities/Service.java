package com.wojtek.CarService.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Accessors(fluent = true)
@Entity
@Table(name = "service")
public class Service {

    @Id
    @GeneratedValue
    private Integer id;

    @Column(name = "kind", nullable = false)
    private String kind;

    @Column(name = "type", nullable = false)
    private String type;

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "price", nullable = false)
    private double price;

    @Column(name = "quantity")
    private int quantity;


    @OneToMany(mappedBy = "service", fetch = FetchType.LAZY)
    private List<Orders> orders = new ArrayList<>();
}
