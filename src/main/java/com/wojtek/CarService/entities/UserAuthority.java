package com.wojtek.CarService.entities;

import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Entity;
import javax.persistence.Id;

@Data
@Accessors(fluent = true)
@Entity(name = "authorities")
public class UserAuthority {

    @Id
    public String username;
    public String authority;

}
