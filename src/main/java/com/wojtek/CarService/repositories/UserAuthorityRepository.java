package com.wojtek.CarService.repositories;

import com.wojtek.CarService.entities.UserAuthority;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserAuthorityRepository extends CrudRepository<UserAuthority, String> {

}
