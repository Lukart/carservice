package com.wojtek.CarService.repositories;

import com.wojtek.CarService.entities.Address;
import org.springframework.data.repository.CrudRepository;

public interface AddressRepository extends CrudRepository<Address, Integer> {
}
