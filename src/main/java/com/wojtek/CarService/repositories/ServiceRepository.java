package com.wojtek.CarService.repositories;

import com.wojtek.CarService.entities.Service;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ServiceRepository extends CrudRepository<Service,Integer> {
    List<Service> findAllByKindAndTypeOrderByName(String kind, String type);
    Service findById(int id);
}
